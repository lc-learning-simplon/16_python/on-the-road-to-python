# On the Road to Python.
# Exercice 1 :

# Coucou
var1 = "Coucou les simploniens"
print(var1)

# Bonjour ...
nom = input('Entrez votre nom : ')
print("Bonjour " + nom)

# Écrire un programme qui demande à l'utilisateur la saisie de a et b et affiche la somme de a et de b.

a = int(input("Donnez une valeur pour a : "))
b = int(input("Donnez une valeur pour b : "))

somme = a + b
print(somme)

# Les triples !
n = 0
var2 = 1
result = []
while n != 12:
  if n == 0:
    result.append(var2)
  else:
    var2 = var2 * 3
    result.append(var2) 
  n += 1
print(result)

# Stars
print("*\n**\n***\n****\n*****\n******\n*******")

# Exercice 2 : 
semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimance"]

# 5 premiers jours
print(semaine[:5])

# 5 derniers jours
print(semaine[-2:])

# Dernier jour
print(semaine[-1])
print(semaine[6])

# Inversion
semaine.reverse()
print(semaine)

# Saisons
printemps = ["Mars", "Avril", "Mai", "Juin"]
ete = ["Juillet", "Août"]
automne = ["Septembre", "Novembre"]
hiver = ["Décembre", "Janvier", "Février"]

saisons = [printemps, ete, automne, hiver]

print(saisons[2])
print(saisons[1][0])
print(saisons[1:2])
print(saisons[:][1])
